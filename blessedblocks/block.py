from .line import Line
from threading import RLock, Event
from collections import namedtuple
import re
import abc
import sys

'''
A Block represents a rectangular section of a terminal screen. It's also a node
in a tree of Block object. If a Block in the tree has a non-empty 'grid' attribute,
then it is non-terminal in the tree, and is not displayed. Conversely, if a block
has an empty 'grid' attribute, it is terminal (a leaf node), and is displayed

A Block's 'grid' attribute contains a Grid object. A Grid specifies how the
Block's children are broken up into rectangular slots, completely
filling out the rectangular slot the parent Block owns.

A SizePref is a declaration of the Block's demands and requests when placed into a
Grid with other Blocks. These are recommendations only, which are satisfied when
possible, but always considered fairly across all blocks. To be completely accurate,
the SizePref does not pertain to the entire grid, but ratheronly the row or column
 of blocks the given block sits in.

Blocks (mutable) and SizePrefs (immutable) are thread-safe, Grids are not.
Grid modifications should be done by replacing one Grid with another.

A Runner (defined in runner.y) is responsible for displaying a Block,
and all the Blocks it contains, recursively, in the terminal. The Runner displays the
Block when started, and again every time any Block changes, or a periodic timer expires.

'''

QueueEntry = namedtuple('QueueEntry', 'cmd, args, kwargs')
SizePref = namedtuple('SizePref', 'hard_min hard_max')

# This default SizePref is maximally cooperative. It will take as much space as you can give
# it (hard_max=float('inf'), ie, no hard max), but if space is at a premium give it to
# other blocks over me if they are requesting it (hard_min=0, ie, no minimum).
DEFAULT_SIZE_PREF = SizePref(hard_min=0, hard_max=float('inf'))
class Grid(object):
    def __init__(self, layout=None, blocks=None, cmds=None, handler=None, frame_spec=None):
        if (layout or blocks) and not (layout and blocks):
            raise ValueError('Grid arguments layout and blocks must both exist or both not exist.')
        self.write_lock = RLock()
        self._raw_blocks = blocks
        self._slots = {}  # ints to blocks
        self._names = {}  # names to blocks (name is int if no name)
        self.blocks = {}  # block names to named blocks
        self.handlers = {}  # grid names to their handlers
        self._layout = layout if layout else []
        if frame_spec:
            self._layout = self.frame(frame_spec)
        self._index = 0
        self._filler = 0
        self._cmds = cmds
        self.handler = handler if handler else Grid.handle(lambda a,b,c,d : None)
        self._stop_event = Event()
        self._load(self._layout, self._raw_blocks)

    def highest_block_number(self):
        def high(layout, out):
            for element in layout:
                if isinstance(element, int):
                    out = max(element, out)
                else:
                    out = max(high(element, out), out)
            return out
        return high(self._layout, 0)

    def frame(self, frame_spec):
        fs = frame_spec
        def p(container, start):
            new = [] if isinstance(container, list) else ()
            for i, element in enumerate(container):
                if i != 0 and i != len(container):
                    new += (start,)
                    if isinstance(new, list):
                        block = VFillBlock(text=fs.inner_pattern, width=fs.inner_width)
                    else:
                        block = HFillBlock(text=fs.inner_pattern, height=fs.inner_height)
                    self._raw_blocks[start] = block
                    start += 1
                if isinstance(element, int):
                    new += (element,)
                else:
                    out, start = p(element, start)
                    new += (out,)
            return new, start

        out, start = p(self._layout, self.highest_block_number() + 1)

        self._raw_blocks[start] = VFillBlock(text=fs.outer_pattern, width=fs.outer_width)
        self._raw_blocks[start+1] = HFillBlock(text=fs.outer_pattern, height=fs.outer_height)
        self._raw_blocks[start+2] = HFillBlock(text=fs.outer_pattern, height=fs.outer_height)
        self._raw_blocks[start+3] = VFillBlock(text=fs.outer_pattern, width=fs.outer_width)
        return [start, (start+1, out, start+2), start+3]

    def _load(self, layout, blocks=None):
        with self.write_lock:
            for element in layout:
                if type(element) == int:
                    if element in self._slots:
                        raise ValueError('numbers embedded in grid must not have duplicates')
                    self._index = max(self._index, element) + 1
                    if blocks and element in blocks:
                        self._slots[element] = blocks[element]
                        name = blocks[element].name
                        if name:
                            self._names[name] = blocks[element]
                            if isinstance(blocks[element], Block):
                                self.blocks[name] = blocks[element]
                            elif blocks[element].handler:
                                self.handlers[name] = blocks[element].handler
                        else:
                            self._names[str(element)] = blocks[element]
                    else:
                        self._slots[element] = None
                        self._names[element] = None
                elif type(element) in (list, tuple):
                    if len(element) == 0:
                        raise ValueError('lists and tuples embedded in grid must not be empty')
                    self._load(element, blocks)
                else:
                    raise ValueError('grid must contain only list of numbers and tuples of numbers')

    def __repr__(self):
        return str(self._layout)

    from functools import wraps
    def handle(f):
        @wraps(f)
        def handled(*args):
            grid, cmd, argz, kwargz = args
            # Set up lifeline for child blocks and grids
            for name, block in grid._names.items():
                if block and not hasattr(block, 'lifeline'):
                    block.lifeline = grid.lifeline
                if block.grid and not hasattr(block.grid, 'lifeline'):
                    block.grid.lifeline = grid.lifeline

            # start and stop messages ALWAYS get pushed all the way down the tree
            if cmd in ('start', 'stop', 'resize'):
                if cmd == 'stop':
                    grid._stop_event.set()
                for name, block in grid._names.items():
                    if block.grid: # it's a grid
                        # send it the stop message
                        if block.grid.handler:
                            block.grid.handler(block.grid, cmd, argz, kwargz)
                    else:  # it's a block
                        if cmd == 'stop':
                            # set the block's stop_event
                            block._stop_event.set()
            # all other messages (other than start and stop) get handled by the grid's handler
            # which may or may not resend the msg to its children
            f(*args)
        return handled

'''
These two wrappers add convenience for keeping the Block thread-safe.
The safe_set function notifies the Grid that the block is contain in
that the block has changed.
'''
from functools import wraps
def safe_set(method):
    @wraps(method)
    def _impl(self, *args, **kwargs):
        with self.write_lock:
            method(self, *args, **kwargs)
        try:
            if self.lifeline:
                self.lifeline.refresh()
        except AttributeError:
            pass
    return _impl

def safe_get(method):
    @wraps(method)
    def _impl(self, *args, **kwargs):
        with self.write_lock:
            r = method(self, *args, **kwargs)
        return r
    return _impl

class Block(object, metaclass=abc.ABCMeta):
    MIDDLE_DOT = u'\u00b7'

    def __init__(self,
                 name=None, # must be unique among blocks in a grid
                 text=None,
                 hjust='<',  # horizontally left-justified within block
                 vjust='^',  # vertically centered within block
                 block_just=True,  # justify block as a whole vs line-by-line
                 # The SizePrefs indicate how much screen real estate (width and height) this
                 # block desires/requires when displayed.
                 w_sizepref = None,
                 h_sizepref = None,
                 grid=None):
        self.write_lock = RLock()
        self.name = name
        self.hjust = hjust
        self.vjust = vjust
        self.block_just = block_just
        self.num_text_rows = 0
        self.num_text_cols = 0
        # num_text_rows and num_text_cols MUST be set before self.text is!
        self.text = text if text else None
        self.w_sizepref = w_sizepref
        self.h_sizepref = h_sizepref
        self.grid = grid
        self._stop_event = Event()
        # Below here non-thread safe attrs: TODO (document or make thread-safe)
        self.prev_seq = ''


    def __repr__(self):
        return ('<Block name={0}>'
                .format(self.name))

    @abc.abstractmethod
    def display(self, width, height, x, y, term=None):
        raise NotImplementedError('Subclasses must define display() in order to use this base class.')

    @property
    @safe_get
    def text(self): return self._text

    @text.setter
    @safe_set
    def text(self, val):
        if not hasattr(self, '_text'):
            self._text = ''
        if val is not None and self._text != val:
            rows = val.split('\n')
            clean_rows = []
            for row in rows:
                clean_rows.append(re.sub(r'{t\..*?}', '', row))
            self.num_text_cols = max(map(len, clean_rows))
            self.num_text_rows = len(clean_rows)

            if self.block_just:
                built_rows = []
                for i, crow in enumerate(clean_rows):
                    built_rows.append(rows[i] + (' ' * (self.num_text_cols - len(crow))))
                self._text = '\n'.join(built_rows)
            else:
                self._text = val

    @property
    @safe_get
    def hjust(self): return self._hjust

    @hjust.setter
    @safe_set
    def hjust(self, val):
        if val not in ('<', '^', '>'):
            raise ValueError("Invalid hjust value, must be '<', '^', or '>'")
        self._hjust = val

    @property
    @safe_get
    def vjust(self): return self._vjust

    @vjust.setter
    @safe_set
    def vjust(self, val):
        if val not in ('^', '=', 'v'):
            raise ValueError("Invalid vjust value, must be '^', '=', or 'v'")
        self._vjust = val

    @property
    @safe_get
    def block_just(self): return self._block_just

    @block_just.setter
    @safe_set
    def block_just(self, val):
        self._block_just = val

    @property
    @safe_get
    def h_sizepref(self): return self._h_sizepref

    @h_sizepref.setter
    @safe_set
    def h_sizepref(self, val): self._h_sizepref = val

    @property
    @safe_get
    def w_sizepref(self): return self._w_sizepref

    @w_sizepref.setter
    @safe_set
    def w_sizepref(self, val):
        self._w_sizepref = val

    @property
    @safe_get
    def grid(self): return self._grid

    @grid.setter
    @safe_set
    def grid(self, val): self._grid = val

    @property
    @safe_get
    def num_text_rows(self): return self._num_text_rows

    @num_text_rows.setter
    @safe_set
    def num_text_rows(self, val): self._num_text_rows = val

    @property
    @safe_get
    def num_text_cols(self): return self._num_text_cols

    @num_text_cols.setter
    @safe_set
    def num_text_cols(self, val): self._num_text_cols = val

    @property
    @safe_get
    def name(self): return self._name

    @name.setter
    @safe_set
    def name(self, val): self._name = val

class VFillBlock(Block):
    def __init__(self, text, width=1, name=None):
        self.width = width
        zero_or_width = 0 if not text else width  # Don't take up space if there's no text
        w_sizepref = SizePref(hard_min=zero_or_width, hard_max=zero_or_width)
        h_sizepref=SizePref(hard_min=0, hard_max=float('-inf'))
        super().__init__(name=name,
                         text=text,
                         w_sizepref=w_sizepref,
                         h_sizepref=h_sizepref)

    def display(self, width, height, x, y, term=None):
        if not self.text:
            if not term:
                return []
            return

        with self.write_lock:
            out = []
            for i in range(self.width):
                lines = Line.repeat_to_height(self.text, height)
                for j in range(height):
                    if term:
                        text = lines[j].display
                        with term.location(x=x+i, y=y+j):
                            print(text.format(t=term), end='')
                    else:
                        # for testing purposes only
                        out.append(lines[j].display)
        return out

    @property
    @safe_get
    def text(self): return self._text

    @text.setter
    @safe_set
    def text(self, val):
        border_text, seqs, last_seq = Line.parse(val)
        Block.text.fset(self, val)
        Block.w_sizepref.fset(self, SizePref(hard_min=len(border_text), hard_max=len(border_text)))

class HFillBlock(Block):
    def __init__(self, text, height=1, name=None):
        self.height = height
        zero_or_height = 0 if not text else height  # Don't take up space if there's no text
        w_sizepref=SizePref(hard_min=zero_or_height, hard_max=float('-inf'))
        h_sizepref=SizePref(hard_min=zero_or_height, hard_max=zero_or_height)
        super().__init__(name=name,
                         text=text,
                         w_sizepref=w_sizepref,
                         h_sizepref=h_sizepref
        )

    def display(self, width, height, x, y, term=None):
        if not self.text:
            if not term:
                return []
            return

        with self.write_lock:
            out = ''
            for i in range(self.height):
                text = Line.repeat_to_width(self.text, width).display
                if term:
                    with term.location(x=x, y=y+i):
                        print(text.format(t=term), end='')
                else:
                  # for testing purposes only
                    out += text
            return [out]

    @property
    @safe_get
    def text(self): return self._text

    @text.setter
    @safe_set
    def text(self, val):
        zero_or_one = 0 if not val else 1  # Don't take up space if there's no text
        Block.text.fset(self, val)
        Block.w_sizepref.fset(self, SizePref(hard_min=zero_or_one, hard_max=float('-inf')))
        Block.h_sizepref.fset(self, SizePref(hard_min=zero_or_one, hard_max=zero_or_one))

class FrameSpec():
    def __init__(self,
                 outer_width=1,
                 outer_height=1,
                 outer_pattern=' ',
                 inner_width=1,
                 inner_height=1,
                 inner_pattern=' '):
        self.outer_width = outer_width
        self.outer_height = outer_height
        self.outer_pattern = outer_pattern
        self.inner_width = inner_width
        self.inner_pattern = inner_pattern
        self.inner_height = inner_height
