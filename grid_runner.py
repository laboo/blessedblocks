#!/usr/bin/python3
import os, sys
sys.path.append(os.path.join(os.path.dirname(__file__), "blessedblocks"))
# Previous two lines not needed if blessedblocks modules is installed
from blessedblocks.block import Block, Grid, SizePref
from blessedblocks.blocks import BareBlock, FramedBlock, InputBlock
from blessedblocks.runner import Runner
from time import sleep
from grid_sample import GridSample2

blocks = {}
blocks[1] = InputBlock()
blocks[2] = BareBlock(name='top', grid=GridSample2())

@Grid.handle
def handler(grid, cmd, args, kwargs):
    subgrid = grid.blocks['top'].grid
    if cmd in ('x', 'abc'):
        subgrid.handler(subgrid, cmd, args, kwargs)

grid = Grid(blocks=blocks, layout=[(1,2)], handler=handler, cmds={'x', 'abc', 'stop'})
r = Runner(grid, stderr_filename='/tmp/testing')
r.start()
r.init_grid(grid) # allows stderr logging to start
sys.stderr.write('hi\n')
r.dump()
while r.heartbeat():
    sys.stderr.write('tick\n')
    r.dump()
    sleep(1)

