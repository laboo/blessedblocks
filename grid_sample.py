#!/usr/bin/python3
import os, sys
sys.path.append(os.path.join(os.path.dirname(__file__), "blessedblocks"))
# Previous two lines not needed if blessedblocks modules is installed
from blessedblocks.block import Block, Grid, SizePref
from blessedblocks.blocks import BareBlock, FramedBlock, InputBlock
from threading import Event, Thread

class GridSample(Grid):
    def __init__(self):
        blocks = {}
        blocks[8] = FramedBlock(BareBlock(name='ef-one'), text='ss', name='f-one', title='Framed1',title_sep='-', top_border='x', bottom_border='z')
        blocks[9] = FramedBlock(BareBlock(name='ef-two'), text='tt',name='f-two', title='Framed2',title_sep='-')

        @Grid.handle
        def handler(grid, cmd, args, kwargs):
            if cmd == 'x':
                grid.blocks['f-one'].text = 'def'
            elif cmd == 'abc':
                grid.blocks['f-one'].text = 'ghi'
                grid.blocks['f-one'].top_border = '{t.blue}o'

        super(GridSample, self).__init__(layout=[8,9],
                                         blocks=blocks,
                                         cmds={'x', 'abc', 'stop'},
                                         handler=handler)
        self._thread = Thread(
            name='sleeper',
            target=self._run,
            args=()
        )
        self._thread.start()
        
    def _run(self):
        import logging            
        while not self._stop_event.wait(1.0):
            pass


class GridSample2(Grid):
    def __init__(self):
        blocks = {}
        blocks[1] = BareBlock(name='mid1', grid=GridSample())
        blocks[2] = BareBlock(name='mid2')

        @Grid.handle
        def handler(grid, cmd, args, kwargs):
            mid1 = grid.blocks['mid1']
            mid2 = grid.blocks['mid2']
            
            if cmd == 'x':
                mid1.grid.handler(mid1.grid, cmd, args, kwargs)
            elif cmd == 'abc':
                mid1.grid.handler(mid1.grid, cmd, args, kwargs)
                mid2.text = 'mmmmidddd'

        super(GridSample2, self).__init__(layout=[1,2],
                                          blocks=blocks,
                                          cmds={'x', 'abc', 'stop'},
                                          handler=handler)
        
